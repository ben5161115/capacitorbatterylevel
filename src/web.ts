import { WebPlugin, registerPlugin } from '@capacitor/core';

import type { BatteryLevelPlugin } from './definitions';

export class BatteryLevelWeb extends WebPlugin implements BatteryLevelPlugin {
    async getBatteryLevel(): Promise<{ value: number }> {
        return { value: 404.404 };
    }
}

const BatteryLevel = new BatteryLevelWeb();
export { BatteryLevel };
registerPlugin('BatteryLevel', BatteryLevel);
