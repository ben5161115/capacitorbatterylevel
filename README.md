# battery-level-cap

Gets battery level of the device

## Install

```bash
npm install battery-level-cap
npx cap sync
```

## API

<docgen-index>

* [`getBatteryLevel()`](#getbatterylevel)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### getBatteryLevel()

```typescript
getBatteryLevel() => Promise<{ value: number; }>
```

**Returns:** <code>Promise&lt;{ value: number; }&gt;</code>

--------------------

</docgen-api>
