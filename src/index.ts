import { registerPlugin } from '@capacitor/core';

import type { BatteryLevelPlugin } from './definitions';

const BatteryLevel = registerPlugin<BatteryLevelPlugin>('BatteryLevel');

export { BatteryLevel };

export * from './definitions';
