package com.mycompany.plugins.example;

import android.content.Context;
import android.os.BatteryManager;
import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;


@NativePlugin(name = "BatteryLevel")
public class BatteryLevelPlugin extends Plugin {

    @PluginMethod()
    public void getBatteryLevel(PluginCall call) {
        BatteryManager batteryManager = (BatteryManager) getContext().getSystemService(Context.BATTERY_SERVICE);
        int batteryLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

        JSObject ret = new JSObject();
        ret.put("value", batteryLevel);
        call.success(ret);
    }
}

