export interface BatteryLevelPlugin {
  getBatteryLevel(): Promise<{value: number}>;
}
